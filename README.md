# Project 4: Brevet time calculator with Ajax

Reimplemented the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points 
where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which 
the rider must arrive at the location.   

The algorithm for calculating controle times is described here 
(https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here 
(https://rusa.org/pages/rulesForRiders). 

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html).  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

This project instead reads in the distances entered from the user, and the open and close times are
filled in using AJAX and Flask automatically.

## Using the program

First, build and run the container in the same directory as the Dockerfile. When the image is being 
built, the nosetests are automatically run.

Then, go to `localhost:5000` in a web browser to access the main page.

You will need to first provide information for the brevet you wish to plan for. On the top left, 
choose a distance (in kilometers, not miles) from the drop-down menu. Then, specify the starting
date and time of the brevet. **Make sure these values are correct before entering any control distances!**
Do not change these after entering control distances as the existing distances will not be updated. If you
do need to change any of these after entering control points, either re-enter each of the control point distances
or reload the page.

You can now enter up to 20 control point distances. Click on an input box, and enter a distance. Then, 
press return to display the open and close times. 

**Note**: You will receive a message if you do any of the following:

* Use a letter in the kilometer field
* Entered a control point greater than 20% the brevet distance

The 20% rule is my attempt at mimicking the RUSA calculator, but I didn't want to throw an error
like RUSA did.

## How the open and close times are calculated

This chart is taken from https://rusa.org/pages/acp-brevet-control-times-calculator

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr)|
|:---------------------:|:---------------------:|:--------------------:|
| 0 - 200               | 15                    | 34                   |
| 200 - 400             | 15                    | 32                   |
| 400 - 600             | 15                    | 30                   |
| 600 - 1000            | 11.428                | 28                   |
| 1000 - 1300           | 13.333                | 26                   |

Important: The overall time limit for each brevet, according to the distance, are
(in hours and minutes, HH:MM) 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 
40:00 for 600 KM, and 75:00 for 1000 KM.

The opening time is calculated as (distance/maximum speed) according to the control location range.
For example, a control point at 100km on a 200km brevet will have an opening time of 100/34 = 2.9412 hours = 2:56
past the opening time. For a control at 550km on a 600km brevet, the opening time will be
200/34 + 200/32 + 150/30 = 17.1324 hours = 17:08. Note how all the rows in the table up to the brevet distance
are used. For control distances greater than the brevet distance, use the brevet distance itself for the final 
calculation. For example, a control point at 405km on a 400km brevet should have an opening time of
200/34 + 200/32 = 12.1324 hours = 12:08.

The closing times are a little more tricky. 

For control points greater than or equal to 60km, the closing time is calculated as (distance/minimum speed)
in the same way as for the open times. For example, a control point at 550km on a 600km brevet would have a
closing time of 200/15 + 200/15 + 150/15 = 36.6667 hours = 36:40. For all controls greater than or equal to the
brevet distance, use the overall time limit given below the table above. For example, a control point at 205km on
a 200km brevet should close in 13:30.

For control points less than 60km, the minimum speed is based off of 20 km/hr plus 1 additional hour. For example,
a control at 20km would have a closing time of 20/20 + 1 = 2.0000 hours = 2:00 past the start time instead of 20/15
as in the table. 

## Contact information

Author: Jimmy Lam, jimmyl@uoregon.edu
