"""
Nose tests for close_time

All results are from the calculator at this link:
    https://rusa.org/octime_acp.html
"""

from acp_times import close_time
import arrow
import nose

START_TIME = arrow.get('2020-01-01 0:00', 'YYYY-MM-DD H:mm')

def get_time(date_time):
    """ get the arrow time from date_time argument

    Args:
        date_time: formatted as 'YYYY-MM-MM H:mm'
    """
    return arrow.get(date_time, 'YYYY-MM-DD H:mm').isoformat()

# Function header for close_time:
# def close_time(control_dist_km, brevet_dist_km, brevet_start_time)

def test_close_200():
    assert close_time(0, 200, START_TIME) == get_time('2020-01-01 1:00')
    assert close_time(1, 200, START_TIME) == get_time('2020-01-01 1:03')
    assert close_time(100, 200, START_TIME) == get_time('2020-01-01 6:40')
    assert close_time(199, 200, START_TIME) == get_time('2020-01-01 13:16')
    assert close_time(200, 200, START_TIME) == get_time('2020-01-01 13:30')
    assert close_time(201, 200, START_TIME) == get_time('2020-01-01 13:30')
    assert not close_time(200, 200, START_TIME) == get_time('2020-01-01 13:20')

    # example 1
    assert close_time(60, 200, START_TIME) == get_time('2020-01-01 4:00')
    assert close_time(120, 200, START_TIME) == get_time('2020-01-01 8:00')
    assert close_time(175, 200, START_TIME) == get_time('2020-01-01 11:40')
    assert close_time(205, 200, START_TIME) == get_time('2020-01-01 13:30')

def test_close_300():
    assert close_time(0, 300, START_TIME) == get_time('2020-01-01 1:00')
    assert close_time(1, 300, START_TIME) == get_time('2020-01-01 1:03')
    assert close_time(100, 300, START_TIME) == get_time('2020-01-01 6:40')
    assert close_time(199, 300, START_TIME) == get_time('2020-01-01 13:16')
    assert close_time(200, 300, START_TIME) == get_time('2020-01-01 13:20')
    assert close_time(201, 300, START_TIME) == get_time('2020-01-01 13:24')
    assert close_time(299, 300, START_TIME) == get_time('2020-01-01 19:56')
    assert close_time(300, 300, START_TIME) == get_time('2020-01-01 20:00')
    assert close_time(301, 300, START_TIME) == get_time('2020-01-01 20:00')

def test_close_400():
    assert close_time(0, 400, START_TIME) == get_time('2020-01-01 1:00')
    assert close_time(1, 400, START_TIME) == get_time('2020-01-01 1:03')
    assert close_time(100, 400, START_TIME) == get_time('2020-01-01 6:40')
    assert close_time(199, 400, START_TIME) == get_time('2020-01-01 13:16')
    assert close_time(200, 400, START_TIME) == get_time('2020-01-01 13:20')
    assert close_time(201, 400, START_TIME) == get_time('2020-01-01 13:24')
    assert close_time(299, 400, START_TIME) == get_time('2020-01-01 19:56')
    assert close_time(300, 400, START_TIME) == get_time('2020-01-01 20:00')
    assert close_time(301, 400, START_TIME) == get_time('2020-01-01 20:04')
    assert close_time(399, 400, START_TIME) == get_time('2020-01-02 2:36')
    assert close_time(400, 400, START_TIME) == get_time('2020-01-02 3:00')
    assert close_time(401, 400, START_TIME) == get_time('2020-01-02 3:00')

def test_close_600():
    assert close_time(0, 600, START_TIME) == get_time('2020-01-01 1:00')
    assert close_time(1, 600, START_TIME) == get_time('2020-01-01 1:03')
    assert close_time(100, 600, START_TIME) == get_time('2020-01-01 6:40')
    assert close_time(199, 600, START_TIME) == get_time('2020-01-01 13:16')
    assert close_time(200, 600, START_TIME) == get_time('2020-01-01 13:20')
    assert close_time(201, 600, START_TIME) == get_time('2020-01-01 13:24')
    assert close_time(299, 600, START_TIME) == get_time('2020-01-01 19:56')
    assert close_time(300, 600, START_TIME) == get_time('2020-01-01 20:00')
    assert close_time(301, 600, START_TIME) == get_time('2020-01-01 20:04')
    assert close_time(399, 600, START_TIME) == get_time('2020-01-02 2:36')
    assert close_time(400, 600, START_TIME) == get_time('2020-01-02 2:40')
    assert close_time(401, 600, START_TIME) == get_time('2020-01-02 2:44')
    assert close_time(500, 600, START_TIME) == get_time('2020-01-02 9:20')
    assert close_time(599, 600, START_TIME) == get_time('2020-01-02 15:56')
    assert close_time(600, 600, START_TIME) == get_time('2020-01-02 16:00')
    assert close_time(601, 600, START_TIME) == get_time('2020-01-02 16:00')

    # example 2
    assert close_time(550, 600, START_TIME) == get_time('2020-01-02 12:40')
    assert close_time(609, 600, START_TIME) == get_time('2020-01-02 16:00')

def test_close_1000():
    assert close_time(0, 1000, START_TIME) == get_time('2020-01-01 1:00')
    assert close_time(1, 1000, START_TIME) == get_time('2020-01-01 1:03')
    assert close_time(100, 1000, START_TIME) == get_time('2020-01-01 6:40')
    assert close_time(199, 1000, START_TIME) == get_time('2020-01-01 13:16')
    assert close_time(200, 1000, START_TIME) == get_time('2020-01-01 13:20')
    assert close_time(201, 1000, START_TIME) == get_time('2020-01-01 13:24')
    assert close_time(299, 1000, START_TIME) == get_time('2020-01-01 19:56')
    assert close_time(300, 1000, START_TIME) == get_time('2020-01-01 20:00')
    assert close_time(301, 1000, START_TIME) == get_time('2020-01-01 20:04')
    assert close_time(399, 1000, START_TIME) == get_time('2020-01-02 2:36')
    assert close_time(400, 1000, START_TIME) == get_time('2020-01-02 2:40')
    assert close_time(401, 1000, START_TIME) == get_time('2020-01-02 2:44')
    assert close_time(500, 1000, START_TIME) == get_time('2020-01-02 9:20')
    assert close_time(599, 1000, START_TIME) == get_time('2020-01-02 15:56')
    assert close_time(600, 1000, START_TIME) == get_time('2020-01-02 16:00')
    assert close_time(601, 1000, START_TIME) == get_time('2020-01-02 16:05')
    assert close_time(750, 1000, START_TIME) == get_time('2020-01-03 5:08')
    assert close_time(999, 1000, START_TIME) == get_time('2020-01-04 2:55')
    assert close_time(1000, 1000, START_TIME) == get_time('2020-01-04 3:00')
    assert close_time(1001, 1000, START_TIME) == get_time('2020-01-04 3:00')

    # example 3
    assert close_time(890, 1000, START_TIME) == get_time('2020-01-03 17:23')
