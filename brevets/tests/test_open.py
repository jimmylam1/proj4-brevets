"""
Nose tests for open_time

All results are from the calculator at this link:
    https://rusa.org/octime_acp.html
"""

from acp_times import open_time
import arrow
import nose

START_TIME = arrow.get('2020-01-01 0:00', 'YYYY-MM-DD H:mm')

def get_time(date_time):
    """ get the arrow time from date_time argument

    Args:
        date_time: formatted as 'YYYY-MM-MM H:mm'
    """
    return arrow.get(date_time, 'YYYY-MM-DD H:mm').isoformat()

# Function header for open_time:
# def open_time(control_dist_km, brevet_dist_km, brevet_start_time)

def test_open_200():
    assert open_time(0, 200, START_TIME) == get_time('2020-01-01 0:00')
    assert open_time(1, 200, START_TIME) == get_time('2020-01-01 0:02')
    assert open_time(199, 200, START_TIME) == get_time('2020-01-01 5:51')
    assert open_time(200, 200, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(201, 200, START_TIME) == get_time('2020-01-01 5:53')
    assert not open_time(201, 200, START_TIME) == get_time('2020-01-01 5:55')

    # example 1
    assert open_time(60, 200, START_TIME) == get_time('2020-01-01 1:46')
    assert open_time(120, 200, START_TIME) == get_time('2020-01-01 3:32')
    assert open_time(175, 200, START_TIME) == get_time('2020-01-01 5:09')
    assert open_time(205, 200, START_TIME) == get_time('2020-01-01 5:53')

def test_open_300():
    assert open_time(0, 300, START_TIME) == get_time('2020-01-01 0:00')
    assert open_time(1, 300, START_TIME) == get_time('2020-01-01 0:02')
    assert open_time(199, 300, START_TIME) == get_time('2020-01-01 5:51')
    assert open_time(200, 300, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(201, 300, START_TIME) == get_time('2020-01-01 5:55')
    assert open_time(299, 300, START_TIME) == get_time('2020-01-01 8:59')
    assert open_time(300, 300, START_TIME) == get_time('2020-01-01 9:00')
    assert open_time(301, 300, START_TIME) == get_time('2020-01-01 9:00')
    assert not open_time(301, 300, START_TIME) == get_time('2020-01-01 9:02')
    assert not open_time(300, 300, START_TIME) == get_time('2020-01-01 9:22')

def test_open_400():
    assert open_time(0, 400, START_TIME) == get_time('2020-01-01 0:00')
    assert open_time(1, 400, START_TIME) == get_time('2020-01-01 0:02')
    assert open_time(100, 400, START_TIME) == get_time('2020-01-01 2:56')
    assert open_time(199, 400, START_TIME) == get_time('2020-01-01 5:51')
    assert open_time(200, 400, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(201, 400, START_TIME) == get_time('2020-01-01 5:55')
    assert open_time(300, 400, START_TIME) == get_time('2020-01-01 9:00')
    assert open_time(399, 400, START_TIME) == get_time('2020-01-01 12:06')
    assert open_time(400, 400, START_TIME) == get_time('2020-01-01 12:08')
    assert open_time(401, 400, START_TIME) == get_time('2020-01-01 12:08')

def test_open_600():
    assert open_time(0, 600, START_TIME) == get_time('2020-01-01 0:00')
    assert open_time(1, 600, START_TIME) == get_time('2020-01-01 0:02')
    assert open_time(100, 600, START_TIME) == get_time('2020-01-01 2:56')
    assert open_time(199, 600, START_TIME) == get_time('2020-01-01 5:51')
    assert open_time(200, 600, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(201, 600, START_TIME) == get_time('2020-01-01 5:55')
    assert open_time(300, 600, START_TIME) == get_time('2020-01-01 9:00')
    assert open_time(399, 600, START_TIME) == get_time('2020-01-01 12:06')
    assert open_time(400, 600, START_TIME) == get_time('2020-01-01 12:08')
    assert open_time(401, 600, START_TIME) == get_time('2020-01-01 12:10')
    assert open_time(500, 600, START_TIME) == get_time('2020-01-01 15:28')
    assert open_time(599, 600, START_TIME) == get_time('2020-01-01 18:46')
    assert open_time(600, 600, START_TIME) == get_time('2020-01-01 18:48')
    assert open_time(601, 600, START_TIME) == get_time('2020-01-01 18:48')

    # example 2
    assert open_time(100, 600, START_TIME) == get_time('2020-01-01 2:56')
    assert open_time(200, 600, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(350, 600, START_TIME) == get_time('2020-01-01 10:34')
    assert open_time(550, 600, START_TIME) == get_time('2020-01-01 17:08')

def test_open_1000():
    assert open_time(0, 1000, START_TIME) == get_time('2020-01-01 0:00')
    assert open_time(1, 1000, START_TIME) == get_time('2020-01-01 0:02')
    assert open_time(100, 1000, START_TIME) == get_time('2020-01-01 2:56')
    assert open_time(199, 1000, START_TIME) == get_time('2020-01-01 5:51')
    assert open_time(200, 1000, START_TIME) == get_time('2020-01-01 5:53')
    assert open_time(201, 1000, START_TIME) == get_time('2020-01-01 5:55')
    assert open_time(300, 1000, START_TIME) == get_time('2020-01-01 9:00')
    assert open_time(399, 1000, START_TIME) == get_time('2020-01-01 12:06')
    assert open_time(400, 1000, START_TIME) == get_time('2020-01-01 12:08')
    assert open_time(401, 1000, START_TIME) == get_time('2020-01-01 12:10')
    assert open_time(500, 1000, START_TIME) == get_time('2020-01-01 15:28')
    assert open_time(599, 1000, START_TIME) == get_time('2020-01-01 18:46')
    assert open_time(600, 1000, START_TIME) == get_time('2020-01-01 18:48')
    assert open_time(601, 1000, START_TIME) == get_time('2020-01-01 18:50')
    assert open_time(785, 1000, START_TIME) == get_time('2020-01-02 1:24')
    assert open_time(975, 1000, START_TIME) == get_time('2020-01-02 8:12')
    assert open_time(999, 1000, START_TIME) == get_time('2020-01-02 9:03')
    assert open_time(1000, 1000, START_TIME) == get_time('2020-01-02 9:05')
    assert open_time(1001, 1000, START_TIME) == get_time('2020-01-02 9:05')

    # example 3
    assert open_time(890, 1000, START_TIME) == get_time('2020-01-02 5:09')
